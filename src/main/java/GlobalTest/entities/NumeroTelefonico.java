package GlobalTest.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the NUMERO_TELEFONICO database table.
 * 
 */
@Entity
@Table(name="NUMERO_TELEFONICO")
@NamedQuery(name="NumeroTelefonico.findAll", query="SELECT n FROM NumeroTelefonico n")
public class NumeroTelefonico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NT_NID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ntNid;

	@Column(name="NT_SCODIGO_CIUDAD")
	private String ntScodigoCiudad;

	@Column(name="NT_SCODIGO_PAIS")
	private String ntScodigoPais;

	@Column(name="NT_SNUMERO")
	private String ntSnumero;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="USU_NID")
	private Usuario usuario;

	public NumeroTelefonico() {
	}

	public int getNtNid() {
		return this.ntNid;
	}

	public void setNtNid(int ntNid) {
		this.ntNid = ntNid;
	}

	public String getNtScodigoCiudad() {
		return this.ntScodigoCiudad;
	}

	public void setNtScodigoCiudad(String ntScodigoCiudad) {
		this.ntScodigoCiudad = ntScodigoCiudad;
	}

	public String getNtScodigoPais() {
		return this.ntScodigoPais;
	}

	public void setNtScodigoPais(String ntScodigoPais) {
		this.ntScodigoPais = ntScodigoPais;
	}

	public String getNtSnumero() {
		return this.ntSnumero;
	}

	public void setNtSnumero(String ntSnumero) {
		this.ntSnumero = ntSnumero;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
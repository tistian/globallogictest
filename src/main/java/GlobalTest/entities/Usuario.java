package GlobalTest.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the USUARIO database table.
 * 
 */
@Entity
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USU_NID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int usuNid;

	@Temporal(TemporalType.DATE)
	@Column(name="USU_DFECHA_CREACION")
	private Date usuDfechaCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="USU_DFECHA_MODIFICACION")
	private Date usuDfechaModificacion;

	@Temporal(TemporalType.DATE)
	@Column(name="USU_DULTIMO_ACCESO")
	private Date usuDultimoAcceso;

	@Column(name="USU_SACTIVO")
	private String usuSactivo;

	@Column(name="USU_SEMAIL")
	private String usuSemail;

	@Column(name="USU_SNOMBRE")
	private String usuSnombre;

	@Column(name="USU_SPASSWORD")
	private String usuSpassword;

	@Column(name="USU_STOKEN")
	private String usuStoken;

	//bi-directional many-to-one association to NumeroTelefonico
	@OneToMany(mappedBy="usuario")
	private List<NumeroTelefonico> numeroTelefonicos;

	public Usuario() {
	}

	public int getUsuNid() {
		return this.usuNid;
	}

	public void setUsuNid(int usuNid) {
		this.usuNid = usuNid;
	}

	public Date getUsuDfechaCreacion() {
		return this.usuDfechaCreacion;
	}

	public void setUsuDfechaCreacion(Date usuDfechaCreacion) {
		this.usuDfechaCreacion = usuDfechaCreacion;
	}

	public Date getUsuDfechaModificacion() {
		return this.usuDfechaModificacion;
	}

	public void setUsuDfechaModificacion(Date usuDfechaModificacion) {
		this.usuDfechaModificacion = usuDfechaModificacion;
	}

	public Date getUsuDultimoAcceso() {
		return this.usuDultimoAcceso;
	}

	public void setUsuDultimoAcceso(Date usuDultimoAcceso) {
		this.usuDultimoAcceso = usuDultimoAcceso;
	}

	public String getUsuSactivo() {
		return this.usuSactivo;
	}

	public void setUsuSactivo(String usuSactivo) {
		this.usuSactivo = usuSactivo;
	}

	public String getUsuSemail() {
		return this.usuSemail;
	}

	public void setUsuSemail(String usuSemail) {
		this.usuSemail = usuSemail;
	}

	public String getUsuSnombre() {
		return this.usuSnombre;
	}

	public void setUsuSnombre(String usuSnombre) {
		this.usuSnombre = usuSnombre;
	}

	public String getUsuSpassword() {
		return this.usuSpassword;
	}

	public void setUsuSpassword(String usuSpassword) {
		this.usuSpassword = usuSpassword;
	}

	public String getUsuStoken() {
		return this.usuStoken;
	}

	public void setUsuStoken(String usuStoken) {
		this.usuStoken = usuStoken;
	}

	public List<NumeroTelefonico> getNumeroTelefonicos() {
		return this.numeroTelefonicos;
	}

	public void setNumeroTelefonicos(List<NumeroTelefonico> numeroTelefonicos) {
		this.numeroTelefonicos = numeroTelefonicos;
	}

	public NumeroTelefonico addNumeroTelefonico(NumeroTelefonico numeroTelefonico) {
		getNumeroTelefonicos().add(numeroTelefonico);
		numeroTelefonico.setUsuario(this);

		return numeroTelefonico;
	}

	public NumeroTelefonico removeNumeroTelefonico(NumeroTelefonico numeroTelefonico) {
		getNumeroTelefonicos().remove(numeroTelefonico);
		numeroTelefonico.setUsuario(null);

		return numeroTelefonico;
	}

}
package GlobalTest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import GlobalTest.bean.ErrorResponse;
import GlobalTest.bean.Phone;
import GlobalTest.bean.UsuarioRequest;
import GlobalTest.bean.UsuarioResponse;
import GlobalTest.entities.NumeroTelefonico;
import GlobalTest.entities.Usuario;
import GlobalTest.repository.PhoneRepository;
import GlobalTest.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PhoneRepository phoneRepository;
	
	@PostMapping("/registraUsuario")
	public ResponseEntity<Object> createUser(@RequestParam(value="comando")  String request) {
		
		

		String regexEmail="^(.+)@(.+)$";
		
		Pattern passPattern = 
		        Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,15})");
		
		
		
		Gson g = new Gson();
		UsuarioRequest userRequest = g.fromJson(request, UsuarioRequest.class);
		ResponseEntity<Object> responseEntity;
		System.out.println("email : "+userRequest.getEmail());
		System.out.println("phones "+userRequest.getPhones());
		


		
		System.out.println("validcion pass : "+passPattern.matcher(userRequest.getPassword()).matches());
		
		
		if(!userRequest.getEmail().matches(regexEmail))
		{
			ErrorResponse error= new ErrorResponse();
			error.setMensaje("Formato de email incorrecto");
			responseEntity= new ResponseEntity<Object>(error,HttpStatus.BAD_REQUEST);
		}
		else if(!passPattern.matcher(userRequest.getPassword()).matches())
		{
			ErrorResponse error= new ErrorResponse();
			error.setMensaje("Formato o largo de password incorrecto");
			responseEntity= new ResponseEntity<Object>(error,HttpStatus.NOT_ACCEPTABLE);
		}else
		{
	
			List<Usuario> usuarios= userRepository.findByUsuSemail( userRequest.getEmail());
			
			if(usuarios.size()>0)
			{
				ErrorResponse error= new ErrorResponse();
				error.setMensaje("correo ya existe");
				responseEntity= new ResponseEntity<Object>(error,HttpStatus.IM_USED);
				
			}else
			{
				Usuario user= new Usuario();
				user.setUsuSemail(userRequest.getEmail());
				user.setUsuSnombre(userRequest.getName());
				user.setUsuSpassword(userRequest.getPassword());
			    user.setUsuSactivo("S");
				user.setUsuDfechaModificacion(new Date());
				user.setUsuDultimoAcceso(new Date());
				user.setUsuDfechaCreacion(new Date());
				
				
			    user= userRepository.save(user);
		
				for (Phone phone : userRequest.getPhones()) {
					
					NumeroTelefonico numeroteleTelefonico= new NumeroTelefonico();
					numeroteleTelefonico.setNtSnumero(phone.getNumber());
					numeroteleTelefonico.setNtScodigoCiudad(phone.getCitycode());
					numeroteleTelefonico.setNtScodigoPais(phone.getContrycode());
					numeroteleTelefonico.setUsuario(user);
					phoneRepository.save(numeroteleTelefonico);
					
				}
		
				UsuarioResponse userResponse=new UsuarioResponse();
				
				UUID uuid = UUID.randomUUID();
				userResponse.setIsActivo(user.getUsuSactivo());
				userResponse.setFechaCreacion(user.getUsuDfechaCreacion());
				userResponse.setFechaModificacion(user.getUsuDfechaModificacion());
				userResponse.setFechaUltimoIngreso(user.getUsuDultimoAcceso());
				userResponse.setUsuarioID(new Long(user.getUsuNid()));
				userResponse.setToken(uuid.toString());
				responseEntity= new ResponseEntity<Object>(userResponse,HttpStatus.OK);
			}
		}
	   return responseEntity;
	}

}

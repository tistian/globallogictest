package GlobalTest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GlobalTest.entities.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Long> {

	List<Usuario> findByUsuSnombreAndUsuSemailAndUsuSpassword(String nombreUsuario,String emailUsuario,String password);
	List<Usuario> findByUsuSemail(String emailUsuario);

}

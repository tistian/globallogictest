package GlobalTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GlobalTest.entities.NumeroTelefonico;

@Repository
public interface PhoneRepository  extends JpaRepository<NumeroTelefonico, Long>{

}


alter table NUMERO_TELEFONICO
   drop constraint FK_NUMERO_T_POSEE_USUARIO;

drop index POSEE_FK;

drop table NUMERO_TELEFONICO cascade constraints;

drop table USUARIO cascade constraints;


create table NUMERO_TELEFONICO 
(
   NT_NID               INTEGER              not null,
   USU_NID              INTEGER,
   NT_SNUMERO           VARCHAR(20),
   NT_SCODIGO_CIUDAD    VARCHAR(4),
   NT_SCODIGO_PAIS      VARCHAR(4),
   constraint PK_NUMERO_TELEFONICO primary key (NT_NID)
);

create index POSEE_FK on NUMERO_TELEFONICO (
   USU_NID ASC
);


create table USUARIO 
(
   USU_NID              INTEGER              not null,
   USU_SNOMBRE          VARCHAR(50),
   USU_SEMAIL           VARCHAR(30),
   USU_SPASSWORD        VARCHAR(20),
   USU_SACTIVO          CHAR(1),
   USU_DFECHA_MODIFICACION DATE,
   USU_DFECHA_CREACION  DATE,
   USU_DULTIMO_ACCESO   DATE,
   USU_STOKEN           VARCHAR(100),
   constraint PK_USUARIO primary key (USU_NID)
);

alter table NUMERO_TELEFONICO
   add constraint FK_NUMERO_T_POSEE_USUARIO foreign key (USU_NID)
      references USUARIO (USU_NID);

